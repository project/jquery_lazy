<?php

/**
 * @file
 * Module file for Lazy-load.
 */

/**
 * The minimum required version of the Colorbox plugin.
 */

define('JQUERY_LAZY_MIN_PLUGIN_VERSION', '1.7.10');

/**
 * Implements hook_init().
 */
function jquery_lazy_init() {
  if (!path_is_admin(current_path())) {
    jquery_lazy_is_library_installed();
  }
}

/**
 * Is jquery.lazy library installed.
 */
function jquery_lazy_is_library_installed() {
  $jquery_lazy = libraries_detect('jquery.lazy');

  $jquery_lazy_installed = variable_get('jquery_lazy_library_installed');

  if ($jquery_lazy['installed'] !== $jquery_lazy_installed) {
    variable_set('jquery_lazy_library_installed', $jquery_lazy['installed']);
  }

  return $jquery_lazy['installed'];
}

/**
 * Implements hook_theme_registry_alter().
 */
function jquery_lazy_theme_registry_alter(&$theme_registry) {
  $jquery_lazy_installed = variable_get('jquery_lazy_library_installed');
  if ($jquery_lazy_installed && !path_is_admin(current_path())) {
    $theme_registry['image']['function'] = 'jquery_lazy_image_wrapper';
  }
}

/**
 * Returns HTML for an image.
 *
 * @param array $variables
 *   An associative array containing:
 *   - path: Either the path of the image file (relative to base_path()) or a
 *     full URL.
 *   - width: The width of the image (if known).
 *   - height: The height of the image (if known).
 *   - alt: The alternative text for text-based browsers. HTML 4 and XHTML 1.0
 *     always require an alt attribute. The HTML 5 draft allows the alt
 *     attribute to be omitted in some cases. Therefore, this variable defaults
 *     to an empty string, but can be set to NULL for the attribute to be
 *     omitted. Usually, neither omission nor an empty string satisfies
 *     accessibility requirements, so it is strongly encouraged for code calling
 *     theme('image') to pass a meaningful value for this variable.
 *     - http://www.w3.org/TR/REC-html40/struct/objects.html#h-13.8
 *     - http://www.w3.org/TR/xhtml1/dtds.html
 *     - http://dev.w3.org/html5/spec/Overview.html#alt
 *   - title: The title text is displayed when the image is hovered in some
 *     popular browsers.
 *   - attributes: Associative array of attributes to be placed in the img tag.
 *
 * @return string
 *   HTML for a lazy image
 */
function jquery_lazy_image_wrapper($variables) {
  static $rdwimages_enabled, $image_placeholder_src;
  $attributes = $variables['attributes'];
  $src = file_create_url($variables['path']);
  $attributes['data-src'] = $src;
  $attributes['class'] = ['lazy-image lazy-loader'];
  foreach (['width', 'height', 'alt', 'title'] as $key) {
    if (isset($variables[$key])) {
      $attributes[$key] = $variables[$key];
    }
  }
  return '<img' . drupal_attributes($attributes) . ' />';
}

/**
 * Implements hook_page_build().
 */
function jquery_lazy_page_build(&$page) {

  $library_installed = variable_get('jquery_lazy_library_installed');

  if ($library_installed && !path_is_admin(current_path())) {

    $path = drupal_get_path('module', 'jquery_lazy');

    $page['page_bottom']['jquery_lazy'] = [
      '#attached' => [],
    ];
    $attached = &$page['page_bottom']['jquery_lazy']['#attached'];
    $attached['libraries_load'][] = ['jquery.lazy'];
    $attached['js'][$path . '/js/lazy.js'] = ['every_page' => TRUE];
    $attached['css'][$path . '/css/style.css'] = ['every_page' => TRUE];
  }
}

/**
 * Implements hook_libraries_info().
 */
function jquery_lazy_libraries_info() {
  $libraries['jquery.lazy'] = [
    'name' => 'jquery.lazy',
    'vendor url' => 'http://jquery.eisbehr.de/lazy/',
    'download url' => 'http://jquery.eisbehr.de/lazy/',
    'version callback' => 'jquery_lazy_library_get_version',
    'version arguments' => ['file' => 'package.json'],
    'files' => [
      'js' => [
        'jquery.lazy.min.js' => [
          'group' => JS_LIBRARY,
          'every_page' => TRUE,
        ],
        'jquery.lazy.plugins.min.js' => [
          'group' => JS_LIBRARY,
          'every_page' => TRUE,
        ],
      ],
    ],
    'variants' => [
      'source' => [
        'files' => [
          'js' => [
            'jquery.lazy.min.js' => [
              'group' => JS_LIBRARY,
              'every_page' => TRUE,
            ],
            'jquery.lazy.plugins.min.js' => [
              'group' => JS_LIBRARY,
              'every_page' => TRUE,
            ],
          ],
        ],
      ],
    ],
  ];

  return $libraries;
}

/**
 * Callback function to return version from provided JSON file.
 */
function jquery_lazy_library_get_version($library, $options) {
  $file = DRUPAL_ROOT . '/' . $library['library path'] . '/' . $options['file'];
  if (empty($options['file']) || !file_exists($file)) {
    return;
  }
  $file_content = file_get_contents($file);
  $json = json_decode($file_content, TRUE);

  return $json['version'];
}
