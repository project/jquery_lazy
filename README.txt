This module will automatically lazy load all images for sites, which will make the site load faster.

All images will only load when it's visible to the browser window.

For other images:

You can also manually lazy load your other images not processed by Drupal image module by formatting your img markup to this:

Attributes:
1) src = path to placeholder image
2) data-src = path to actual image
3) width = add width for best result
4) height = add height for best result

Requirements
Libraries API module (Drupal 7 only)
jQuery.Lazy v1.7.10 script as a library item

Installing Manually
Download jquery.lazy from https://github.com/eisbehr-/jquery.lazy/archive/master.zip
Extract the downloaded file,
rename directory to jquery.lazy
copy the folder into the libraries folder.

In Drupal 7, sites/all/libraries folder;