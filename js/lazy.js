(function ($) {

  'use strict';

  Drupal.behaviors.lazy = {
    attach: function (context, settings) {
  	  $('.lazy-image').lazy({
          afterLoad: function(element) {
            element.removeClass('lazy-loader');
          },
      });
    }
  };

})(jQuery);
